resource "aws_s3_bucket" "default" {
  bucket = "${var.bucket_name}"
  acl    = "public-read"
  policy = "${data.template_file.bucket_policy.rendered}"

  website {
    index_document = "${var.index_doc}"
    error_document = "${var.error_doc}"
  }

  tags = "${var.tags}"
}

data "template_file" "bucket_policy" {
  template = "${file("${path.module}/policy.json.tpl")}"

  vars {
    bucket_name = "${var.bucket_name}"
  }
}
