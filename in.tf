variable "bucket_name" {
  description = "The name of the S3 bucket to store website objects, such as FQDN."
}

variable "root_doc" {
  description = "Root document such as index.html."
  default     = "index.html"
}

variable "error_doc" {
  description = "Error document."
  default     = "index.html"
}

variable "tags" {
  type        = "map"
  description = "Map of resource tags. Suggest using tf_resource_tags."
}
