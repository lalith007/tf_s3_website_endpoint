# S3 Website Endpoint

Terraform module to create a S3 Bucket with a website endpoint.

## Usage

Unlike a website, the endpoint does not have logging or www -> @ redirection, etc. Combine with CloudFront for bonafide website hosting.

```terraform
provider "aws" {
  region = "${var.region}"
}

module "resource_tags" {
  source = "git::https://gitlab.com/tom.davidson/tf_resource_tags.git"
}

module "s3-website" {
  source = "git::https://gitlab.com/tom.davidson/tf_s3_website_endpoint.git"

  bucket_name = "domain.com"
  tags = "${module.resource_tags.map}"

  # optional
  index_doc = "index.html"
  error_doc = "error.html"
}
```

## Next

- Custom policy as var
- Optional/conditional logging
